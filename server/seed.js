const mongoose = require('mongoose'),
      config = require('./libs/config'),
      User = require('./models/user'),
      Article = require('./models/article');

// TODO only do this if in dev mode
module.exports = app => {
  User.remove({},
   function(error){
    Article.remove({},
     function(error){


      // Real coordinates for some locations in Trondheim, Karmøy, Stavanger etc...
      const coordinates = [ [ '10.338365','63.398148' ],  [ '-0.119543','51.503324' ],  [ '10.3666851','63.4029872' ],  [ '5.735607','58.963019' ],  [ '10.388039','63.421038' ],  [ '10.1612977','63.3902608' ],  [ '7.1378547','58.1876538' ],  [ '10.4581814','63.4251725' ],  [ '10.3761','63.40408' ],  [ '10.39529','63.4197' ],  [ '10.39183','63.4285538' ],  [ '5.6417681','58.9349011' ],  [ '49.0660806','55.8304307' ],  [ '44.5133034','48.708048' ],  [ '10.3922436','63.429778' ],  [ '-0.0201672','51.520331' ],  [ '10.3946319','63.4336566' ],  [ '5.7489502','58.9685027' ],  [ '5.641063','58.93638' ],  [ '-0.1820629','51.1536621' ],  [ '5.6906363','58.9619556' ],  [ '5.7026131','58.9784762' ],  [ '5.7331933','58.9713871' ],  [ '48.3837914','54.3181598' ],  [ '5.7187238','58.9724487' ],  [ '5.713092','58.9721136' ],  [ '10.382807','63.4223033' ],  [ '7.4099494','59.7694988' ],  [ '10.4453231','63.4181807' ],  [ '10.4494926','63.422378' ],  [ '10.3765762','63.4135415' ],  [ '10.4245916','63.38978' ],  [ '10.4179403','63.414272' ],  [ '5.7398486','58.9726784' ],  [ '5.283204','59.400747' ],  [ '10.4321177','63.4332875' ],  [ '10.4129487','63.4140772' ],  [ '5.6415977','58.9347992' ],  [ '5.308487','59.283153' ],  [ '10.3150754','63.3774522' ],  [ '55.9720554','54.7387621' ],  [ '-95.677068','37.0625' ],  [ '5.73555','58.96346' ],  [ '56.2667916','58.0296813' ],  [ '5.7021934','58.9538017' ],  [ '48.3977999','54.3166' ],  [ '10.4319344','63.4223565' ],  [ '10.7946984','59.8463397' ],  [ '5.7204453','58.7875931' ],  [ '5.7493476','58.9563181' ],  [ '10.3811492','63.407159' ],  [ '-0.0983956','51.5230488' ],  [ '10.3983229','63.4106927' ],  [ '10.4125451','63.3980494' ],  [ '10.4018216','63.4178575' ],  [ '5.5947234','59.1376537' ],  [ '5.2738822','59.4100395' ],  [ '10.4047526','63.4167575' ],  [ '10.3936304','63.4298921' ],  [ '10.3607087','63.4080625' ],  [ '37.6173','55.755826' ],  [ '10.42978','63.43874' ],  [ '10.4023918','63.4359055' ],  [ '5.745981','58.957533' ],  [ '10.4471794','63.4260718' ],  [ '6.65015','59.892401' ],  [ '10.3764739','63.4047257' ],  [ '10.3933972','63.3630728' ],  [ '10.3981169','63.4159735' ],  [ '5.7420683','58.9393379' ],  [ '-0.1908463','51.5021532' ],  [ '10.3667797','63.4206424' ],  [ '5.6993018','58.9551105' ],  [ '5.7200926','58.969918' ],  [ '13.0172043','66.1943582' ],  [ '5.6572702','58.9520202' ],  [ '10.3963975','63.4310429' ],  [ '5.4777023','59.6135782' ],  [ '5.7510962','58.9593783' ],  [ '5.7161762','58.9699019' ],  [ '-0.1549671','51.5228901' ],  [ '5.739336','58.964605' ],  [ '5.7580637','58.9640964' ],  [ '10.4359186','63.4118376' ],  [ '10.3757353','63.4279922' ],  [ '10.4708427','63.4089983' ],  [ '5.71299','58.952519' ],  [ '48.397248','54.316974' ],  [ '10.402077','63.419499' ],  [ '5.7393102','58.9646009' ],  [ '10.3965315','63.4322072' ] ]

      // Gives you a random int between min and max.
      function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min) + min);
      }

      // Array of articles with link to its corresponding image
      var articlenameWithFoto = [
        {name: 'pen',               imgLink: ['https://www.randomlists.com/img/things/pen.jpg']},
        {name: 'water-bottle',      imgLink: ['https://www.randomlists.com/img/things/water_bottle.jpg']},
        {name: 'tissue box',        imgLink: ['https://www.randomlists.com/img/things/tissue_box.jpg']},
        {name: 'bow',               imgLink: ['https://www.randomlists.com/img/things/bow.jpg']},
        {name: 'legwarmers',        imgLink: ['https://www.randomlists.com/img/things/leg_warmers.jpg']},
        {name: 'bananas',           imgLink: ['https://www.randomlists.com/img/things/bananas.jpg']},
        {name: 'soda-can',          imgLink: ['https://www.randomlists.com/img/things/soda_can.jpg']},
        {name: 'thermostat',        imgLink: ['https://www.randomlists.com/img/things/thermostat.jpg']},
        {name: 'toilet',            imgLink: ['https://www.randomlists.com/img/things/toilet.jpg']},
        {name: 'book',              imgLink: ['https://www.randomlists.com/img/things/chapter_book.jpg']},
        {name: 'candle',            imgLink: ['https://www.randomlists.com/img/things/candle.jpg']},
        {name: 'toe-ring',          imgLink: ['https://www.randomlists.com/img/things/toe_ring.jpg']},
        {name: 'sofa',              imgLink: ['https://www.randomlists.com/img/things/sofa.jpg']},
        {name: 'videogame',         imgLink: ['https://www.randomlists.com/img/things/video_games.jpg']},
        {name: 'carrots',           imgLink: ['https://www.randomlists.com/img/things/carrots.jpg']},
        {name: 'pencil',            imgLink: ['https://www.randomlists.com/img/things/pencil.jpg']},
        {name: 'sharpie',           imgLink: ['https://www.randomlists.com/img/things/sharpie.jpg']},
        {name: 'scotchtape',        imgLink: ['https://www.randomlists.com/img/things/scotch_tape.jpg']},
        {name: 'hair brush',        imgLink: ['https://www.randomlists.com/img/things/hair_brush.jpg']},
        {name: 'balloon',           imgLink: ['https://www.randomlists.com/img/things/balloon.jpg']},
        {name: 'teddies',           imgLink: ['https://www.randomlists.com/img/things/teddies.jpg']},
        {name: 'plastic fork',      imgLink: ['https://www.randomlists.com/img/things/plastic_fork.jpg']},
        {name: 'stickynote',        imgLink: ['https://www.randomlists.com/img/things/sticky_note.jpg']},
        {name: 'lotion',            imgLink: ['https://www.randomlists.com/img/things/lotion.jpg']},
        {name: 'wallet',            imgLink: ['https://www.randomlists.com/img/things/wallet.jpg']},
        {name: 'keyboard',          imgLink: ['https://www.randomlists.com/img/things/keyboard.jpg']},
        {name: 'greeting-card',     imgLink: ['https://www.randomlists.com/img/things/greeting_card.jpg']},
        {name: 'brocolli',          imgLink: ['https://www.randomlists.com/img/things/brocolli.jpg']},
        {name: 'stockings',         imgLink: ['https://www.randomlists.com/img/things/stockings.jpg']},
        {name: 'ring',              imgLink: ['https://www.randomlists.com/img/things/ring.jpg']},
        {name: 'sketchpad',         imgLink: ['https://www.randomlists.com/img/things/sketch_pad.jpg']},
        {name: 'icecube-tray',      imgLink: ['https://www.randomlists.com/img/things/ice_cube_tray.jpg']},
        {name: 'controller',        imgLink: ['https://www.randomlists.com/img/things/controller.jpg']},
        {name: 'washingmachine' ,   imgLink: ['https://www.randomlists.com/img/things/washing_machine.jpg']},
        {name: 'phone',             imgLink: ['https://www.randomlists.com/img/things/phone.jpg']},
        {name: 'glass',             imgLink: ['https://www.randomlists.com/img/things/glass.jpg']},
        {name: 'glasses',           imgLink: ['https://www.randomlists.com/img/things/glasses.jpg']},
        {name: 'drillpress',        imgLink: ['https://www.randomlists.com/img/things/drill_press.jpg']},
        {name: 'CD',                imgLink: ['https://www.randomlists.com/img/things/cd.jpg']},
        {name: 'lace',              imgLink: ['https://www.randomlists.com/img/things/lace.jpg']},
        {name: 'hanger',            imgLink: ['https://www.randomlists.com/img/things/hanger.jpg']},
        {name: 'packingpeanuts',    imgLink: ['https://www.randomlists.com/img/things/packing_peanuts.jpg']},
        {name: 'socks',             imgLink: ['https://www.randomlists.com/img/things/socks.jpg']},
        {name: 'creditcard',        imgLink: ['https://www.randomlists.com/img/things/credit_card.jpg']},
        {name: 'stopsign',          imgLink: ['https://www.randomlists.com/img/things/stop_sign.jpg']},
        {name: 'watch',             imgLink: ['https://www.randomlists.com/img/things/watch.jpg']},
        {name: 'milk',              imgLink: ['https://www.randomlists.com/img/things/milk.jpg']},
        {name: 'monitor',           imgLink: ['https://www.randomlists.com/img/things/monitor.jpg']},
        {name: 'speakers',          imgLink: ['https://www.randomlists.com/img/things/speakers.jpg']},
        {name: 'tomato',            imgLink: ['https://www.randomlists.com/img/things/tomato.jpg']},
        {name: 'sunglasses',        imgLink: ['https://www.randomlists.com/img/things/sun_glasses.jpg']},
        {name: 'gridpaper',         imgLink: ['https://www.randomlists.com/img/things/grid_paper.jpg']},
        {name: 'bag',               imgLink: ['https://www.randomlists.com/img/things/bag.jpg']},
        {name: 'streetlights',      imgLink: ['https://www.randomlists.com/img/things/street_lights.jpg']},
        {name: 'keys',              imgLink: ['https://www.randomlists.com/img/things/keys.jpg']},
        {name: 'towel',             imgLink: ['https://www.randomlists.com/img/things/towel.jpg']},
        {name: 'doll',              imgLink: ['https://www.randomlists.com/img/things/doll.jpg']},
        {name: 'shawl',             imgLink: ['https://www.randomlists.com/img/things/shawl.jpg']},
        {name: 'slipper',           imgLink: ['https://www.randomlists.com/img/things/slipper.jpg']},
        {name: 'rubberduck',        imgLink: ['https://www.randomlists.com/img/things/rubber_duck.jpg']},
        {name: 'twister',           imgLink: ['https://www.randomlists.com/img/things/twister.jpg']},
        {name: 'candywrapper',      imgLink: ['https://www.randomlists.com/img/things/candy_wrapper.jpg']},
        {name: 'thread',            imgLink: ['https://www.randomlists.com/img/things/thread.jpg']},
        {name: 'table',             imgLink: ['https://www.randomlists.com/img/things/table.jpg']},
        {name: 'computer',          imgLink: ['https://www.randomlists.com/img/things/computer.jpg']},
        {name: 'remote',            imgLink: ['https://www.randomlists.com/img/things/remote.jpg']},
        {name: 'rubberband',       imgLink: ['https://www.randomlists.com/img/things/rubber_band.jpg']},
        {name: 'hairtie',           imgLink: ['https://www.randomlists.com/img/things/hair_tie.jpg']},
        {name: 'clamp',             imgLink: ['https://www.randomlists.com/img/things/clamp.jpg']},
        {name: 'modelcar',          imgLink: ['https://www.randomlists.com/img/things/model_car.jpg']},
        {name: 'canvas',            imgLink: ['https://www.randomlists.com/img/things/canvas.jpg']},
        {name: 'puddle',            imgLink: ['https://www.randomlists.com/img/things/puddle.jpg']},
        {name: 'needle',            imgLink: ['https://www.randomlists.com/img/things/needle.jpg']},
        {name: 'lamp',              imgLink: ['https://www.randomlists.com/img/things/lamp.jpg']},
        {name: 'couch',             imgLink: ['https://www.randomlists.com/img/things/couch.jpg']},
        {name: 'pants',             imgLink: ['https://www.randomlists.com/img/things/pants.jpg']},
        {name: 'seatbelt',          imgLink: ['https://www.randomlists.com/img/things/seat_belt.jpg']},
        {name: 'rug',               imgLink: ['https://www.randomlists.com/img/things/rug.jpg']},
        {name: 'mousepad',          imgLink: ['https://www.randomlists.com/img/things/mouse_pad.jpg']},
        {name: 'shampoo',           imgLink: ['https://www.randomlists.com/img/things/shampoo.jpg']},
        {name: 'blanket',           imgLink: ['https://www.randomlists.com/img/things/blanket.jpg']},
        {name: 'shovel',            imgLink: ['https://www.randomlists.com/img/things/shovel.jpg']},
        {name: 'shoes',             imgLink: ['https://www.randomlists.com/img/things/shoes.jpg']},
        {name: 'radio',             imgLink: ['https://www.randomlists.com/img/things/radio.jpg']},
        {name: 'lampshade',         imgLink: ['https://www.randomlists.com/img/things/lamp_shade.jpg']},
        {name: 'whiteout',          imgLink: ['https://www.randomlists.com/img/things/white_out.jpg']},
        {name: 'purse',             imgLink: ['https://www.randomlists.com/img/things/purse.jpg']},
        {name: 'mop',               imgLink: ['https://www.randomlists.com/img/things/mop.jpg']},
        {name: 'camera',            imgLink: ['https://www.randomlists.com/img/things/camera.jpg']},
        {name: 'earser',            imgLink: ['https://www.randomlists.com/img/things/earser.jpg']},
        {name: 'box',               imgLink: ['https://www.randomlists.com/img/things/box.jpg']},
        {name: 'house',             imgLink: ['https://www.randomlists.com/img/things/house.jpg']},
        {name: 'outlet',            imgLink: ['https://www.randomlists.com/img/things/outlet.jpg']},
        {name: 'glowstick',         imgLink: ['https://www.randomlists.com/img/things/glow_stick.jpg']},
        {name: 'floor',             imgLink: ['https://www.randomlists.com/img/things/floor.jpg']},
        {name: 'cat',               imgLink: ['https://www.randomlists.com/img/things/cat.jpg']},
        {name: 'photoalbum',       imgLink: ['https://www.randomlists.com/img/things/photo_album.jpg']},
        {name: 'piano',             imgLink: ['https://www.randomlists.com/img/things/piano.jpg']},
        {name: 'vase',              imgLink: ['https://www.randomlists.com/img/things/vase.jpg']},
        {name: 'sponge',            imgLink: ['https://www.randomlists.com/img/things/sponge.jpg']},
        {name: 'ipod',              imgLink: ['https://www.randomlists.com/img/things/ipod.jpg']},
        {name: 'sidewalk',          imgLink: ['https://www.randomlists.com/img/things/sidewalk.jpg']},
        {name: 'cinderblock',       imgLink: ['https://www.randomlists.com/img/things/cinder_block.jpg']},
        {name: 'rusty nail',        imgLink: ['https://www.randomlists.com/img/things/rusty_nail.jpg']},
        {name: 'sandpaper',         imgLink: ['https://www.randomlists.com/img/things/sand_paper.jpg']},
        {name: 'desk',              imgLink: ['https://www.randomlists.com/img/things/desk.jpg']},
        {name: 'helmet',            imgLink: ['https://www.randomlists.com/img/things/helmet.jpg']},
        {name: 'fridge',            imgLink: ['https://www.randomlists.com/img/things/fridge.jpg']},
        {name: 'bread',             imgLink: ['https://www.randomlists.com/img/things/bread.jpg']},
        {name: 'USB-drive',         imgLink: ['https://www.randomlists.com/img/things/usb_drive.jpg']},
        {name: 'spring',            imgLink: ['https://www.randomlists.com/img/things/spring.jpg']},
        {name: 'apple',             imgLink: ['https://www.randomlists.com/img/things/apple.jpg']},
        {name: 'pool-stick',        imgLink: ['https://www.randomlists.com/img/things/pool_stick.jpg']},
        {name: 'wagon',             imgLink: ['https://www.randomlists.com/img/things/wagon.jpg']},
        {name: 'twezzers',          imgLink: ['https://www.randomlists.com/img/things/twezzers.jpg']},
        {name: 'buckel',            imgLink: ['https://www.randomlists.com/img/things/buckel.jpg']},
        {name: 'thermometer',       imgLink: ['https://www.randomlists.com/img/things/thermometer.jpg']},
        {name: 'conditioner',       imgLink: ['https://www.randomlists.com/img/things/conditioner.jpg']},
        {name: 'paper',             imgLink: ['https://www.randomlists.com/img/things/paper.jpg']},
        {name: 'boom-box',          imgLink: ['https://www.randomlists.com/img/things/boom_box.jpg']},
        {name: 'cork',              imgLink: ['https://www.randomlists.com/img/things/cork.jpg']},
        {name: 'clock',             imgLink: ['https://www.randomlists.com/img/things/clock.jpg']},
        {name: 'toothbrush',        imgLink: ['https://www.randomlists.com/img/things/toothbrush.jpg']},
        {name: 'shoelace',         imgLink: ['https://www.randomlists.com/img/things/shoe_lace.jpg']},
        {name: 'knife',             imgLink: ['https://www.randomlists.com/img/things/knife.jpg']},
        {name: 'bowl',              imgLink: ['https://www.randomlists.com/img/things/bowl.jpg']},
        {name: 'headphones',        imgLink: ['https://www.randomlists.com/img/things/headphones.jpg']},
        {name: 'checkbook',         imgLink: ['https://www.randomlists.com/img/things/checkbook.jpg']},
        {name: 'door',              imgLink: ['https://www.randomlists.com/img/things/door.jpg']},
        {name: 'charger',           imgLink: ['https://www.randomlists.com/img/things/charger.jpg']},
        {name: 'tire-swing',        imgLink: ['https://www.randomlists.com/img/things/tire_swing.jpg']},
        {name: 'tree',              imgLink: ['https://www.randomlists.com/img/things/tree.jpg']},
        {name: 'picture-frame',     imgLink: ['https://www.randomlists.com/img/things/picture_frame.jpg']},
        {name:'packet of soy-sauce',imgLink: ['https://www.randomlists.com/img/things/soy_sauce_packet.jpg']},
        {name: 'cell-phone',        imgLink: ['https://www.randomlists.com/img/things/cell_phone.jpg']},
        {name: 'food',              imgLink: ['https://www.randomlists.com/img/things/food.jpg']},
        {name: 'soap',              imgLink: ['https://www.randomlists.com/img/things/soap.jpg']},
        {name: 'pillow',            imgLink: ['https://www.randomlists.com/img/things/pillow.jpg']},
        {name: 'cookie-jar',        imgLink: ['https://www.randomlists.com/img/things/cookie_jar.jpg']},
        {name: 'eye-liner',         imgLink: ['https://www.randomlists.com/img/things/eye_liner.jpg']},
        {name: 'lip-gloss',         imgLink: ['https://www.randomlists.com/img/things/lip_gloss.jpg']},
        {name: 'spoon',             imgLink: ['https://www.randomlists.com/img/things/spoon.jpg']},
        {name: 'toothpaste',        imgLink: ['https://www.randomlists.com/img/things/toothpaste.jpg']},
        {name: 'key-chain',         imgLink: ['https://www.randomlists.com/img/things/key_chain.jpg']},
        {name: 'drawer',            imgLink: ['https://www.randomlists.com/img/things/drawer.jpg']},
        {name: 'bookmark',          imgLink: ['https://www.randomlists.com/img/things/bookmark.jpg']},
        {name: 'book',              imgLink: ['https://www.randomlists.com/img/things/book.jpg']},
        {name: 'bottle-cap',        imgLink: ['https://www.randomlists.com/img/things/bottle_cap.jpg']},
        {name: 'mp3-player',        imgLink: ['https://www.randomlists.com/img/things/mp3_player.jpg']},
        {name: 'chocolate',         imgLink: ['https://www.randomlists.com/img/things/chocolate.jpg']},
        {name: 'money',             imgLink: ['https://www.randomlists.com/img/things/money.jpg']},
        {name: 'bed',               imgLink: ['https://www.randomlists.com/img/things/bed.jpg']},
        {name: 'sailboat',          imgLink: ['https://www.randomlists.com/img/things/sailboat.jpg']},
        {name: 'magnet',            imgLink: ['https://www.randomlists.com/img/things/magnet.jpg']},
        {name: 'fork',              imgLink: ['https://www.randomlists.com/img/things/fork.jpg']},
        {name: 'cup',               imgLink: ['https://www.randomlists.com/img/things/cup.jpg']},
        {name: 'flag',              imgLink: ['https://www.randomlists.com/img/things/flag.jpg']},
        {name: 'plate',             imgLink: ['https://www.randomlists.com/img/things/plate.jpg']},
        {name: 'tooth-pick',        imgLink: ['https://www.randomlists.com/img/things/tooth_picks.jpg']},
        {name: 'car',               imgLink: ['https://www.randomlists.com/img/things/car.jpg']},
        {name: 'face-wash',         imgLink: ['https://www.randomlists.com/img/things/face_wash.jpg']},
        {name: 'perfume',           imgLink: ['https://www.randomlists.com/img/things/perfume.jpg']},
        {name: 'fake flowers',      imgLink: ['https://www.randomlists.com/img/things/fake_flowers.jpg']},
        {name: 'chalk',             imgLink: ['https://www.randomlists.com/img/things/chalk.jpg']},
        {name: 'coasters',          imgLink: ['https://www.randomlists.com/img/things/coasters.jpg']},
        {name: 'button',            imgLink: ['https://www.randomlists.com/img/things/button.jpg']},
        {name: 'tv',                imgLink: ['https://www.randomlists.com/img/things/tv.jpg']},
        {name: 'sandal',            imgLink: ['https://www.randomlists.com/img/things/sandal.jpg']},
        {name: 'mirror',            imgLink: ['https://www.randomlists.com/img/things/mirror.jpg']},
        {name: 'paintbrush',        imgLink: ['https://www.randomlists.com/img/things/paint_brush.jpg']},
        {name: 'bottle',            imgLink: ['https://www.randomlists.com/img/things/bottle.jpg']},
        {name: 'claypot',           imgLink: ['https://www.randomlists.com/img/things/clay_pot.jpg']},
        {name: 'shirt',             imgLink: ['https://www.randomlists.com/img/things/shirt.jpg']},
        {name: 'deodorant',         imgLink: ['https://www.randomlists.com/img/things/deodorant_.jpg']},
        {name: 'screw',             imgLink: ['https://www.randomlists.com/img/things/screw.jpg']},
        {name: 'chair',             imgLink: ['https://www.randomlists.com/img/things/chair.jpg']},
        {name: 'newspaper',         imgLink: ['https://www.randomlists.com/img/things/newspaper.jpg']},
        {name: 'playing card',      imgLink: ['https://www.randomlists.com/img/things/playing_card.jpg']},
        {name: 'truck',             imgLink: ['https://www.randomlists.com/img/things/truck.jpg']},
        {name: 'beef',              imgLink: ['https://www.randomlists.com/img/things/beef.jpg']},
        {name: 'clothes',           imgLink: ['https://www.randomlists.com/img/things/clothes.jpg']},
        {name: 'flowers',           imgLink: ['https://www.randomlists.com/img/things/flowers.jpg']},
        {name: 'bracelet',          imgLink: ['https://www.randomlists.com/img/things/bracelet.jpg']},
        {name: 'window',            imgLink: ['https://www.randomlists.com/img/things/window.jpg']},
        {name: 'air-freshener',     imgLink: ['https://www.randomlists.com/img/things/air_freshener.jpg']},
        {name: 'blouse',            imgLink: ['https://www.randomlists.com/img/things/blouse.jpg']},
        {name: 'zipper',            imgLink: ['https://www.randomlists.com/img/things/zipper.jpg']},
        {name: 'nail-file',         imgLink: ['https://www.randomlists.com/img/things/nail_file.jpg']},
        {name: 'television',        imgLink: ['https://www.randomlists.com/img/things/television.jpg']},
        {name: 'nail-clippers',     imgLink: ['https://www.randomlists.com/img/things/nail_clippers.jpg']}
      ];

      // Verbs, Adjectives and emotions for funny-text generation
      var verbs = ['cooking','smelling','watching','flower','join','reduce','suffer','shade','tour','overflow','whisper','slow','precede','branch','coach','lock','prefer','kill','scream','work','confuse','fool','pass','yawn','shock','save','camp','man up','depend','stare','describe','warm','water','introduce','recognise','repair','tease','knot','exist','trap','identify','train','follow','hum','cough','program','preserve','rush','entertain','divide','box','whine','last','shiver','snore','relax','explain','obey','paste','grin','appear','strengthen'];
      var adjectives = ('smelly,amazing,sloppy,splendid,coordinated,five,rare,obscene,electric,cowardly,enthusiastic,gorgeous,tricky,thoughtless,helpful,daffy,voracious,adhesive,laughable,boorish,maniacal,curved,literate,premium,innocent,married,able,lopsided,fair,brief,wary,melodic,abnormal,defeated,vulgar,caring,scientific,keen,abashed,one,delicate,attractive,moldy,yellow,excellent,enchanted,hissing,well-off,upset,absorbed,heavy,exciting,shivering,untidy,knowing,grey,early,discreet,selfish,plant,windy,tasteless,mindless,nonchalant,blue,half,massive,flagrant,dry,gainful,busy,complete,tidy,offbeat,wiggly,neighborly,ratty,quizzical,cloudy,light,absorbing,probable,cheerful,female,overconfident,fortunate,scintillating,certain,even,yummy,better,pleasant,versed,male,rightful,dazzling,long-term,absent,ablaze,strange,overrated,two,living,incredible,eatable,needless,fabulous,gray,quarrelsome,kindhearted,seemly,unwritten,plastic,free,economic,crabby,near,handy,upbeat,energetic,acceptable,well-groomed,spectacular,gaping,savory,classy,wiry,drab,poised,utopian,possible,parallel,slimy,acidic,alert,reflective,callous,mighty,automatic,ready,profuse,tan,loose,red,null,jobless,wet,mammoth,hard-to-find,sable,guttural,past,low,clever,small,heartbreaking,sticky,level,chubby,hurt,accurate,fearless,tasteful,jazzy,onerous,jealous,lively,ragged,obnoxious,talented,reminiscent,aspiring,deep,unsuitable,tranquil,awesome,scarce,incandescent,fumbling,sassy,shiny,nippy,animated,aboriginal,flaky,teeny-tiny,earthy,befitting,dashing,relieved,new,vigorous,green,threatening,round,addicted,abundant,tacky,languid,cluttered,dead,hairless,sadistic,metal,wild,domesticated,abnormal,medicated,cocky,massive,disrespectful,impressive,out of control,internet worthy,hilarious,sexy,hot,very tactful,bearded,duck-like,violent,slimy,insanely creepy,embarrassed to the bone,self-centered,talking,naked,angry,shaky,deep,sick,zippy,sticky,fluffy,frozen,unholy,painfully honest,filthy,fighting,bonkers,harsh,frisky,greedy,crawly,insane,hideous,ungodly,abusive,drunken,hateful,idiotic,twisted,useless,yapping,magical,indecent,godawful,arrogant,confused,flirting,high-end,insecure,maniacal,sickened,slippery,stubborn,tripping,vengeful,sinister,costumed,cowardly,haunting,startled,alcoholic,demanding,shivering,offensive,nighttime,startling,disgusting,slap happy,disturbing,adulterous,blathering,flickering,rebellious,impertinent,bull headed,hyperactive,infuriating,outnumbered,pea-brained,territorial,underhanded,zombie like,mischievous,at-the-ready,free-loading,house-broken,up-to-no-good,cruel-hearted,misunderstood,narrow-minded,self-absorbed,bat-shit-crazy,fiercely-loyal,out-of-control,fear-inspiring,bat shit crazy,mentally impaired').split(',');
      var emotions = ['sad','happy','angry','jealous','joyous','disgusted','surprised','bored'];

      // Generator for short funny-text.
      function funnyText(ArticleName) {
        let emotion = getRandomInt(0,emotions.length);
        let verb = getRandomInt(0,verbs.length);
        return `${ArticleName} for ${emotions[emotion]} ${verbs[verb]}ing`
      }

      // Generator for a paragraph of funny text.
      function funnyDescription(thing) {
        let emotion = emotions[getRandomInt(0,emotions.length)];
        let emotion2 = emotions[getRandomInt(0,emotions.length)];
        let verb = verbs[getRandomInt(0,verbs.length)];
        let adjective = adjectives[getRandomInt(0,adjectives.length)];
        let adjective2 = adjectives[getRandomInt(0,adjectives.length)];
        // Chose a random flavour for some variation in the article-descriptions.
        let flavour = getRandomInt(1, 10 +1);
        switch (flavour){
          case 1:
          return  `What would you do if you had ${thing}? I will tell you what I would... I would be ${adjective} all the time. It would be absolutely ${adjective2}. And that is why I want you to borrow this ${thing}.`;
          case 2:
          return `When you ${verb}, it is extremely important to have a ${thing}. This ${thing} is ${adjective} for you! I will let you borrow it for a ${adjective2} price.`;
          case 3:
          return `Do you feel ${adjective}? ...No? Then borrow my ${thing}! I guarantee that you will feel ${adjective} all the time.`;
          case 4:
          return `Are you ${emotion}? Then try this ${thing}. No doubt about it, this ${thing} will make you ${adjective}.`;
          case 5:
          return `In the mood for something ${adjective}? This ${thing} is absolutely ${adjective}.`;
          case 6:
          return `This ${thing} is not ${adjective} enough for me. Please borrow it.`;
          case 7:
          return `Are you ${emotion}? Borrow this ${thing}. All your friends will be ${emotion2}.`;
          case 8:
          return `Look at this ${adjective} ${thing}. You know you want it.`;
          case 9:
          return `Well... it's ${adjective}. Need I say more?`;
          case 10:
          return `${adjective} and ${adjective2}. Perfect for a ${verb} party!`;
          default:
          return `${adjective} ${thing} is perfect if you want to ${verb}. Borrow it now for ${adjective2} experience.`;
        }
      }



      // #################################################//
      // .......This is not used for the time beeing......//
      //(not) -- Add two users and three articles -- (not)//
      // #################################################//
      //
      // function randomIntFromInterval(min,max)
      // {
      //   return Math.random()*(max-min+1)+min;
      // }
      //
      // var generationTable = [
      //   {
      //     name: 'Kaffetrakter',
      //     intro: 'knallsterk kaffe',
      //     txt: 'Kaffetrakter som er to eller tre år gammel. sort kaffe',
      //     imgLink: ['http://www.clasohlson.com/medias/sys_master/8885964701726.jpg'],
      //     tags: ['iamatag', 'inventionfuel', 'styrkedrikk'],
      //     index: 0,
      //   },
      //   {
      //     name: 'støvsuger',
      //     intro: 'Suger skikkelig',
      //     txt: 'Støvsuger som er to eller tre år gammel. Liker ikke sokker',
      //     imgLink: ['http://www.indiancustomers.in/sites/default/files/styles/large/public/Super-Elite-1400-B.jpeg'],
      //     tags: ['iamatag', 'støv', 'støvsuger'],
      //     index: 0,
      //   },
      //   {
      //     name: 'ødelakt mikro',
      //     intro: 'fin surrelyd',
      //     txt: 'Varmer ikke opp mat, men lager en beroligende surrelyd',
      //     imgLink: ['https://mystuffrecycled.files.wordpress.com/2014/07/3.jpg'],
      //     tags: ['iamatag', 'whitenoise', 'nofoodtoday'],
      //     index: 0,
      //   },
      //   {
      //     name: 'sofa',
      //     intro: 'Godt brukt, men veldig god',
      //     txt: 'Gått i arv i generasjoner. Lukter litt, men du skal få med klesklyper',
      //     imgLink: ['http://www.advancejunkremoval.com/wp-content/uploads/2011/12/old-couch-removal.jpg'],
      //     tags: ['brun', 'støv', 'sofa'],
      //     index: 0,
      //   }
      // ]

// Adding two users with 1000 articles each. ...  ... ... Money making machines...
      let user = new User({
        email: 'mariusomoe@gmail.com',
        password: 'test',
        profile: { firstName: 'Marius', lastName: 'Moe' },
        confirmation_string: 'pVMoHwTDhqXZmkp9PUwSQ2GKsWDb6i0vAwH8nABM',
        confirmation_issued: new Date(),
        active: true
      });
      user.save(function(err, thisUser) {
        if (err) { console.error("Cant save user" + err) }
        // TODO: Dont send this if the email fails
        console.log("Add testUser 1")
        for (var i = 0; i<1000; i++) {
          var articleType = getRandomInt(0,articlenameWithFoto.length);
          var randomplace = getRandomInt(0,coordinates.length);
          var latitude = coordinates[randomplace][1];
          var longitude = coordinates[randomplace][0];
          // articlenameWithFoto[articleType].index += 1;
          let a = new Article({
            name: `${adjectives[getRandomInt(0,adjectives.length)]} ${articlenameWithFoto[articleType].name}`,
            intro:  funnyText(articlenameWithFoto[articleType].name),
            txt: funnyDescription(articlenameWithFoto[articleType].name),
            price: Math.floor(Math.random() * (300 - 2 + 1)) + 2, // random between 300 and 2
            imgLink: articlenameWithFoto[articleType].          imgLink,
            tags: [
              `${emotions[getRandomInt(0,emotions.length)]}`,
              `${adjectives[getRandomInt(0,adjectives.length)]}`,
              `${verbs[getRandomInt(0,verbs.length)]}`
            ],
            _owner: thisUser._id,
            fromDate: new Date(),
            toDate: new Date(),
            active: true,
            location: {
              lat: latitude,
              lng: longitude
            }
          });
          a.save(function(err, articleOne) {
            if (err) { console.error("Cant add article" + err) }
          })
        }
      })

      let userTwo = new User({
        email: 'marius_oscar@live.no',
        password: 'supertest',
        profile: { firstName: 'Marius oscar', lastName: 'Moee' },
        confirmation_string: 'pVMoHwTDhqXZmkp9PUwSQ2GKsWDb6i0vAwH8nAMM',
        confirmation_issued: new Date(),
        active: true
      });
      userTwo.save(function(err, userTwo) {
        if (err) { console.error("Cant save user" + err) }
        // TODO: Dont send this if the email fails
        console.log("Add testUser 2")
        User.findOne({ email: 'mariusomoe@gmail.com' },
        function(err, existingUser) {

          for (var i = 0; i<1000; i++) {
            var articleType = getRandomInt(0,articlenameWithFoto.length);
            var randomplace = getRandomInt(0,coordinates.length);
            var latitude = coordinates[randomplace][1];
            var longitude = coordinates[randomplace][0];
            // articlenameWithFoto[articleType].index += 1;
            let a = new Article({
              name: `${adjectives[getRandomInt(0,adjectives.length)]} ${articlenameWithFoto[articleType].name}`,
              intro:  funnyText(articlenameWithFoto[articleType].name),
              txt: funnyDescription(articlenameWithFoto[articleType].name),
              price: Math.floor(Math.random() * (300 - 2 + 1)) + 2, // random between 300 and 2
              imgLink: articlenameWithFoto[articleType].          imgLink,
              tags: [
                `${emotions[getRandomInt(0,emotions.length)]}`,
                `${adjectives[getRandomInt(0,adjectives.length)]}`,
                `${verbs[getRandomInt(0,verbs.length)]}`
              ],
              _owner: existingUser._id,
              fromDate: new Date(),
              toDate: new Date(),
              active: true,
              location: {
                lat: latitude,
                lng: longitude
              }
            });
            a.save(function(err, articleOne) {
              if (err) { console.error("Cant add article" + err) }
            })
          }
          console.log("Ready. Website up and running on: http://localhost:4200/login");
        });
      });
    });
  });
}
