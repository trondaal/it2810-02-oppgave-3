const express = require('express'),
      app = express(),
      path = require('path');

app.set('port', 3000);

// creates a static path so one can easily get static files :)
app.use(express.static(path.join(__dirname, '../client-angular')));

// app.get('/', function (req, res) {
//   res.status(200).sendFile(path.join(__dirname, '../client-angular/docs'));
// });
app.listen(app.get('port'), function() {
  console.log('Express docserver listening on port ' + app.get('port'));
});


app.use(function(req, res, next) {
  res.status(404).send('Sorry cant find that!');
});
