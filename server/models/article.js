// Importing Node packages required for schema
const mongoose = require('mongoose'),
      Schema = mongoose.Schema

//================================
// Article Schema
//================================
const ArticleSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  intro: {
    type: String,
    required: true
  },
  txt: {
    type: String,
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  imgLink: [String],
  tags: [String],
  _owner: { type: Schema.Types.ObjectId, ref: 'User' },
  // _category: { type: Schema.ObjectId, ref: 'User' }, // TODO need to implement categories
  fromDate: Date,
  toDate: Date,
  active: {
    type: Boolean,
    default: false
  },
  lat: { type: Number},
  lng: { type: Number},
  location: {
    lat: Number,
    lng: Number
  }

});
module.exports = mongoose.model('Article', ArticleSchema);
