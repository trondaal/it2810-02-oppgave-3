# How to run project on your computer
Install dependencies:
- [Node.js](https://nodejs.org/en/)
- [MongoDB](https://www.mongodb.com/)

Start mongoDB Server:
	`mongod`

Clone the repo:
	`git@bitbucket.org:trondaal/it2810-02-oppgave-3.git`

Go into folder “_it2810-02-oppgave-3/server_” and run:
`npm install
`
This may take some time.


Then start the server:
`npm start
`

The server should now be up and running.
