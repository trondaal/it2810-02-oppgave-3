"use strict";

const jwt = require('jsonwebtoken'),
      User = require('../models/user'),
      Article = require('../models/article'),
      config = require('../libs/config'),
      status = require('../status');


// Set user info from request
function setUserInfo(request) {
  let getUserInfo = {
    _id: request._id,
    firstName: request.profile.firstName,
    lastName: request.profile.lastName,
    email: request.email,
    role: request.role
  };

  return getUserInfo;
}

//========================================
// All articles Route
//========================================

// TODO: Find a way to cache on this end as well.
exports.allArticles = function(req, res, next) {
  Article.find({}, (err, articles) => {
    if (err) { return next(err); }
    // Respond with all the articles
    var timestamp = articles[articles.length-1]._id.getTimestamp();
    // TODO: CHANGE THIS TIMESTAMP WITH LAST KNOWN UPDATE.
    // LAST KNOWN UPDATE WILL BE GIVEN BY ROUTES FOR UPDATE,
    // DELETE OR NEW ARTICLE. IT CURRENTLY GIVES TIMESTAMP
    // OF LAST ENTRY IN THE DATABASE.
    return res.status(200).send({articles: articles, timestamp: timestamp});
  })
  // return res.status(418).send({ message: 'Something must have gone wrong!', status: 9999 });
}

exports.articleById = function(req, res, next) {
  let id = JSON.parse(req.query.articleID);
  let userId = req.user._id

  //this is the db search, Article is the database schema/table
  Article.findById(id, (err, article) => {
    if (err) {
      return next(err);
    }
    User.findById(userId, (err, user) => {
      // If the article being looked at hasn't been looked at before, add it
      // to our recentlyViewed list. If its there already, do nothing.
      if (user.recentlyViewed.indexOf(article._id) == -1) {
        // article does not exist in our recentlyViewed list; add it:
        user.recentlyViewed.push(article._id) // push adds to the END of the array
        if (user.recentlyViewed.length>3){
          user.recentlyViewed.shift(); // shift if we have more than 3
        }
        user.save(function(err) {
          if (err) { console.error("Cant push" + err) }
        });
      }
    });
    // Respond with a filtered list of articles
    return res.status(200).send({article: article});
  });
}

//========================================
// Recently viewed
//========================================
// Returns a list with the three latest viewed articles.
exports.recentlyViewed = function(req, res, next) {
  let token = req.user._id;
  User.findById(token, (err, user) => {
    if (err) { return next(err); }
    Article.find({'_id': {$in: user.recentlyViewed}}, (err, articles) => {
      if (err) { return next(err); }
      return res.status(200).send({articles: articles})
    })

  })
}

//========================================
// Filtered articles
//========================================
// Returns articles based on provided query
exports.filteredArticles = function(req, res, next) {
  //console.log(req.query);
  let filterOptions = JSON.parse(req.query.filterOptions);  // json object
  let fromIndex = JSON.parse(req.query.fromIndex);          // int
  let sorting = req.query.sorting;
  if (((typeof(filterOptions) == 'undefined') || (filterOptions == null))
      || ((typeof(fromIndex) == 'undefined') || (fromIndex == null))) {
        // console.log("\nUnprocessable Entity ERROR\n")
        return res.status(422);
        // http://stackoverflow.com/questions/3050518/what-http-status-response-code-should-i-use-if-the-request-is-missing-a-required
  }

  // use filterOptions to create our database query

  let query = {
    name: {'$regex': filterOptions.name, '$options' : 'i' } , // regex for our string. options: i sets INsensitive to case.
    // price: { '$gte': filterOptions.price[0], '$lte': filterOptions.price[1] },
    tags: filterOptions.tags.length >0 && { '$elemMatch': { '$in': filterOptions.tags } } || {'$regex': '.*'},
    // TODO: Add the rest of the filters
  }

  // console.log(sorting);

  Article.find(query, (err, articles) => {
    if (err) { return next(err); }
    articles.sort(function(a, b) {
      switch (sorting) {
        case 'price':
          return a.price - b.price;
        case 'name':
          if (a.name < b.name) { return -1; }
          if (a.name > b.name) { return  1; }
          return 0;
        default:
          return 0;
      }

    });
    var totalArticles = articles.length; // before splice
    var returnedArticles = articles.splice(fromIndex,50); // only send first 50
    // console.log("ARTICLES:\n")
    // console.log(articles);
    // Respond with a filtered list of articles
    return res.status(200).send({articles: returnedArticles, index: fromIndex,  totalArticles: totalArticles});
  })
  // return res.status(418).send({ message: 'Something must have gone wrong!', status: 9999 });
}


//========================================
// Add one article
//========================================
// Add one article with this user as owner
exports.addArticle = function(req, res, next) {
  let userInfo = setUserInfo(req.user);
  let newArticle = req.body;
  // Check that the new article is not empty.
  if ((typeof(newArticle) == 'undefined') ) {
        // console.log("\nUnprocessable Entity ERROR\n")
        return res.status(422);
  }
  // console.log("Try to add article");
  // console.log(newArticle);

  // Check for bad article.
  if (newArticle.name == '' || newArticle.intro == '' || newArticle.txt == '' ||
      !(typeof(parseInt(newArticle.price)) == "number")) {
        console.error("Article format is wrong");
        return res.status(422).send({message: 'The article is not formated right, is the price a number? ' + newArticle, status: 4567})
      }

  let article = new Article({
    name: newArticle.name,
    intro:  newArticle.intro,
    txt: newArticle.txt,
    price: parseInt(newArticle.price),
    imgLink: newArticle.imgLink,
    tags: newArticle.tags,
    _owner: userInfo._id,
    fromDate: new Date(),
    toDate: new Date(),
    active: newArticle.active,
      location: {
        lat: newArticle.lat,
        lng: newArticle.lng
      }
  });

  // Save the new article
  article.save(function(err){
    if (err) {
      // console.error("cant add article:  " + err);
      return res.status(400).send({message: 'Something is wrong. Could not add article: ' + error, status: 2345});
    }
    return res.status(200).send({articles: 'all good so far!'});
  });

}
