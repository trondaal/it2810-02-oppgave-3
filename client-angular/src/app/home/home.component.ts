import { Component } from '@angular/core';



/**
 * HomeComponent
 *
 * This component holds the first page greeting the user upon logging in.
 */
@Component({
    selector: 'HomeComponent',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css'],
})

export class HomeComponent {}
