import { Component, Input, Output, EventEmitter } from '@angular/core';

import { User, Article } from '../_models/index';





/**
 * ArticleComponent takes (input) Article Model and holds it. This model
 * object is used to render a full box view of the article.
 */
@Component({
    selector: 'ArticleComponent',
    templateUrl: './article.component.html',
    styleUrls: ['./article.component.css'],
})
export class ArticleComponent {
    /**
     * article input, of type Article Model. injected into the component.
     */
    @Input() article: Article;
    /**
     * tag output. EventEmitter, which can emit (see onTagClick).
     */
    @Output() tag: EventEmitter<string> = new EventEmitter<string>();

    /**
     * onTagClick is called when clicking a tag button
     * @param e  MouseEvent
     * @param s  String, the string representing the tag
     * it uses the eventEmitter (which then emits using the
     * (at) output annotation to output to the parent component),
     * and sends the tag string.
     */


    onTagClick(e:MouseEvent, s: string) {
      this.tag.emit(s);
    }


}

/**
 * ArticleSmallComponent takes (input) holds an Article model object. This model
 * object is used to render a small view of the article.
 */
@Component({
    selector: 'ArticleSmallComponent',
    templateUrl: './articleSmall.component.html',
    styleUrls: ['./article.component.css'],
})
export class ArticleSmallComponent {
    /**
     * article input, of type Article Model. injected into the component.
     */
    @Input() article: Article;


}
