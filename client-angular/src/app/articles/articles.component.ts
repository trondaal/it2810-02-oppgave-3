import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { User, Article } from '../_models/index';
import { ArticlesService, FilterOptions } from '../_services/index';
import { OrderByPipe } from '../shared/pipes/orderBy';




/**
 * ArticlesComponent
 *
 * Holds a list of articles, and displays filtering options,
 * sorting options, listing mode options, as well as the list of articles held
 * by articles.service.
 *
 * Implements OnInit, OnDestroy
 */
@Component({
    selector: 'ArticlesComponent',
    templateUrl: './articles.component.html',
    styleUrls: ['./articles.component.css'],
})
export class ArticlesComponent implements OnInit, OnDestroy {
    /**
     * articles holds all articles requested from the service
     */
    private articles: Article[] = [];
    /**
     * loading holds the state whether or not we are currently requesting
     * articles
     */
    private loading: boolean = false;
    /**
     * index holds the current min index of our articles list. Used when
     * requesting more articles given the same filtering.
     */
    private index: number = 0;
    /**
     * totalKnownArticles holds the total number of articles that matched the
     * filter query (which can be more than the number of articles held by the
     * service or the component)
     */
    private totalKnownArticles: number = 0;
    /**
     * listingModes holds the modes in which the articles can be listed,
     * as well as an icon that represents this listing mode.
     */
    private readonly listingModes = [
      {type: 'Box', icon: 'view_module',  },
      {type: 'List', icon: 'view_headline', }
    ];
    /**
     * currentListingMode holds the index of the listingMode currently used.
     */
    private currentListingMode: number;
    /**
     * filterOptions is an object which follows the contract of the
     * FilterOptions interface, and it stores our filters.
     * It is used to store the filter data which is then sent to the API server.
     */
    private filterOptions: FilterOptions;

    /**
     * sortBy holds the option for what articles should be sorted by
     */
    private sortBy: string = 'price';

    /**
     * sortingOptions list with possible sortBy values
     */
    private sortingOptions: string[] = ['price', 'name'];


    // -------------------------------------------------------------------------
    //  CONSTRUCTOR
    // -------------------------------------------------------------------------


    /**
     * constructor
     *
     * Constructor for ArticlesComponent
     * Initializes variables with default, or from localStorage if that exists.
     * @param articlesService  ArticlesService
     * The ArticlesService is injected into ArticlesComponent.
     */
    constructor(private articlesService: ArticlesService) {
      // Advanced TODO: change default of 0 here to 1 for mobile devices
      this.currentListingMode = JSON.parse(localStorage.getItem('listingMode')) || 0;
      this.index = JSON.parse(localStorage.getItem('searchIndex')) || 0;
      // Uses localStorage to cache our filters.
      // the filters cache is destroyed when
      // the component is destroyed. See below.
      this.filterOptions =
        JSON.parse(localStorage.getItem('filterOptions')) ||
        {
          id: '', name: '', intro: '',
          txt: '', tags: [], owner: '',
          price: [0,0], catref: '',
          fromDate: new Date(),
          // Should be default - one month befor and one month after
          toDate: new Date(),
          active: true,
        };
      this.sortBy = JSON.parse(localStorage.getItem('sortOption')) || 'price';
    }

    /**
     * ngOnInit
     *
     * ArticlesComponent implements OnInit
     * OnInit queries for articles once the component is ready
     * See getArticles.
     * If we reload, we should get back our previous, yet updated, state
     */
    ngOnInit() {
      //
      // so we need to query x number of times until we are back at index
      let numQueries = Math.ceil(this.index / 50);
      for (var i = 0; i <= numQueries; i++) {
        this.getArticles(i*50);
      }
    }



    /**
     * ngOnDestroy
     *
     * ArticlesComponent implements OnDestroy
     * ngOnDestroy is called when the component is destroyed
     * Removes filterOptions, listingMode and searchIndex from localStorage
     */
    ngOnDestroy() {
      // the filters cache is destroyed when
      // the component is destroyed.
      localStorage.removeItem('filterOptions');
      localStorage.removeItem('listingMode');
      localStorage.removeItem('searchIndex');
      localStorage.removeItem('sortOption');

    }

    // -------------------------------------------------------------------------
    //  getArticles
    // -------------------------------------------------------------------------

    /**
     * getArticles
     *
     * getArticles requests articles from articles.service
     * @param index  offset to use for the query.
     * Index of 0 gets the first 50 articles.
     * Index of 50 gets articles [50, ..., 99]
     */
    private getArticles(index: number) {
      // use the filterOptions to filter
      this.loading = true;
      this.articlesService.getArticles(this.filterOptions, this.sortBy, index).subscribe(result => {
        this.articles = result;
        this.totalKnownArticles = this.articlesService.getTotalKnownArticles();
        this.loading = false;
      });
    }

    /**
     * getMoreArticles
     *
     * getMoreArticles requests articles from articles.service
     * using index + 50
     * See getArticles(index)
     */
    private getMoreArticles() {
      this.index += 50;
      this.getArticles(this.index);
    }

    // -------------------------------------------------------------------------
    //  Filtering
    // -------------------------------------------------------------------------

    /**
     * setNameFilter
     *
     * setNameFilter sets the filterOptions name filter.
     * Stores filterOptions in localStorage
     * requests articles using getArticles(0);
     * @param s  the filter to be used.
     */
    private setNameFilter(s: string) {
      if (s == this.filterOptions.name) {
        return; // do nothing if nothing changed.
      }
      this.filterOptions.name = s;
      localStorage.setItem('filterOptions', JSON.stringify(this.filterOptions));
      this.getArticles(0);
    }

    /**
     * setTagFilter
     *
     * setTagFilter adds or removes param to the filterOptions tags whitelisting list.
     * Stores filterOptions in localStorage
     * requests articles using getArticles(0);
     * @param s  the tag (string) to be added or removed.
     */
    private setTagFilter(s: string) {
      // remove if it exists, else add.
      var index = this.filterOptions.tags.indexOf(s);
      if (index >= 0) {
        this.filterOptions.tags.splice(index, 1);
      } else {
        this.filterOptions.tags.push(s);
      }
      localStorage.setItem('filterOptions', JSON.stringify(this.filterOptions));
      this.getArticles(0);
    }

    /**
     * applyFilters
     *
     * Applies all filters.
     * Stores filterOptions in localStorage
     * requests articles using getArticles(0);
     */
    private applyFilters() {
      localStorage.setItem('filterOptions', JSON.stringify(this.filterOptions));
      this.getArticles(0);
    }


    /**
     * setListingMode
     *
     * setListingMode changes the listing mode of the displayed articles
     * @param index   the index of the mode to be used.
     */
    private setListingMode(index: number) {
      this.currentListingMode = index;
      localStorage.setItem('listingMode', JSON.stringify(index));
    }

    private setSortOption(sortBy){
      this.sortBy = sortBy;
      localStorage.setItem('sortOption', JSON.stringify(this.sortBy));
      this.getArticles(0)


    }


}
