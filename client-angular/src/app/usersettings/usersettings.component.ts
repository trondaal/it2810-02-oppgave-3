import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../_models/index';
import { AuthenticationService } from '../_services/authentication.service';
import { Subscription }   from 'rxjs/Subscription';



/**
 * UsersettingsComponent
 *
 * Holds the settings of the user.
 */
@Component({
    selector: 'UsersettingsComponent',
    templateUrl: './usersettings.component.html',
    styleUrls: ['./usersettings.component.css'],
})
export class UsersettingsComponent {

  private userSub;
  private user: User;
  private email: string = "";

  /**
   * constructor
   *
   * Constructor for UsersettingsComponent
   * Initializes variable user from the AuthenticationService
   * @param AuthenticationService  AuthenticationService
   * The AuthenticationService is injected into UserPanelComponent.
   */
  constructor(
    private router: Router,
    private service: AuthenticationService) {
    this.userSub = this.service.getCurrentUserObservable().subscribe(user => {
      this.user = user; // Subscribe and get user from the authService
    })
  }

  /**
   * change Email of the current user
   *
   * Change the email to the value provided
   */
  changeEmail(value: string) {
    // console.log(value);
      this.service.changeEmail(value)
          .subscribe(result => {
              if (result === true) {
                  this.router.navigate(['/login']);
              } else {
                  // console.log("Error - You cant change your email right now")
              }
          });
  }


  /**
   * Delete the current user
   *
   * Delete the user that is currently loged in
   * The AuthenticationService then handles the actual logging in.
   */
  delete() {
      this.service.deleteAccoutn()
          .subscribe(result => {
              if (result === true) {
                  this.router.navigate(['/login']);
              } else {
                  // console.log("Error - your account might not have been deleted")
              }
          });
  }

}
