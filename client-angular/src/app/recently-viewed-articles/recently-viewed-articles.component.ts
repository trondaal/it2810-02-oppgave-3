import { Component, OnInit } from '@angular/core';
import { ArticlesService } from "../_services/articles.service";
import { Article } from "../_models/article";
import {Router, NavigationStart, Event as NavigationEvent} from "@angular/router";


/**
 * RecentlyViewedArticlesComponent
 *
 * Display the three moast recently watched articles
 */
@Component({
  selector: 'app-recently-viewed-articles',
  templateUrl: './recently-viewed-articles.component.html',
  styleUrls: ['./recently-viewed-articles.component.css']
})
export class RecentlyViewedArticlesComponent implements OnInit {
  private articles: Article[] = [];
  article: Article

  /**
   * constructor
   *
   * Listen for change in route and if the route changes to root. The moast
   * recently watched items will be updated.
   *
   * @param articlesService  ArticlesService
   * @param router Router
   */
  constructor(private articlesService: ArticlesService,
              private router: Router) {
                router.events.forEach((event: NavigationEvent) => {
                if(event instanceof NavigationStart) {
                  if (event.url=="/"){
                    this.getRecentlyViewed()
                  }
                }
              });
  }

  ngOnInit() {
    this.getRecentlyViewed();
  }



  /**
   * getRecentlyViewed
   *
   * getRecentlyViewed requests articles from articles.service
   *
   */
  private getRecentlyViewed() {
    this.articlesService.getRecentlyViewedArticles().subscribe(result => {
      this.articles = result
      //this.articles = result;
    });
  }
}
