import { Component, OnInit } from "@angular/core";
import { ArticlesService, FilterOptions } from "../_services/articles.service";
import { Article } from "../_models/article";
import {ActiveArticleMapService} from "../_services/active.map.service";
import { Location } from "@angular/common";

/**
 * MapViewComponent
 *
 * Show articles on a map
 */
@Component ({
    selector:"MapViewComponent",
    templateUrl:"./map.component.html",
    styleUrls:["./map.component.css"]
})

export class MapViewComponent{
    lat: number = 63.4186676;
    lng: number = 10.2986818;

    activeArticle: Article;

    /**
     * articles holds all articles requested from the service
     */
    private articles: Article[] = [];

    /**
     * index holds the current min index of our articles list. Used when
     * requesting more articles given the same filtering.
     */
    private index: number = 0;


    /**
     * filterOptions is an object which follows the contract of the
     * FilterOptions interface, and it stores our filters.
     * It is used to store the filter data which is then sent to the API server.
     */
    private filterOptions: FilterOptions;

    private sortBy: string = 'price';

    private sortingOptions: string[] = ['price', 'name'];


    constructor(
        private articlesService: ArticlesService,
        private activeArticleMapService: ActiveArticleMapService,
        private _location: Location,
    ){
      this.index = JSON.parse(localStorage.getItem('searchIndex')) || 0;
      // Uses localStorage to cache our filters.
      // the filters cache is destroyed when
      // the component is destroyed. See below.
      this.filterOptions =
        JSON.parse(localStorage.getItem('filterOptions')) ||
        {
          id: '', name: '', intro: '',
          txt: '', tags: [], owner: '',
          price: [0,0], catref: '',
          fromDate: new Date(),
          // Should be default - one month befor and one month after
          toDate: new Date(),
          active: true,
        };
      this.sortBy = JSON.parse(localStorage.getItem('sortOption')) || 'price';
    }

    ngOnInit(): void{
        this.articlesService.getArticles(this.filterOptions, this.sortBy, this.index)
            .subscribe(articles => {
              this.articles = articles;
            });
    }

  /**
   * Method to set the active/clicked marker on the map
   *
   * @param article : the article to be set as active
   * @param index   : the index number of this marker in the list of markers
   */
  clickedMarker(article: Article, index: number): void {
    // console.log("clicked , " + index );
    this.activeArticleMapService.setActiveArticle(article);
    this.activeArticle = article;

  }

  onBackClick(){
    // console.log("clicked back button");
    this._location.back();

  }

}
