import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FileUploader } from 'ng2-file-upload';

import { Article } from '../_models/index';
import { ArticlesService } from '../_services/index';
import { environment } from '../../environments/environment';

import { JwtHelper } from 'angular2-jwt';


/**
 * AddarticleComponent
 *
 * Holds the component that let users add articles.
 *
 * Implements OnInit
 */
@Component({
  selector: 'app-addarticle',
  templateUrl: './addarticle.component.html',
  styleUrls: ['./addarticle.component.css']
})
export class AddarticleComponent implements OnInit {

    /**
     * model holds the article to be added
     */
    model: Article = new Article();

    /**
     * loading holds the state whether or not we are currently requesting
     * articles
     */
    loading = false;

    /**
     * Holds an error message if any
     */
    error = '';

    /**
     * imageName holds the name of the image that is uploaded
     */
    imageName = '';

    /**
     * areYouReady is used so one will not simply add articles without uploading
     * a picture.
     */
    areYouReady = false;

    /**
     * uploader holds settings used by the FileUploadModule
     */
    public uploader:FileUploader = new FileUploader({url: environment.URL.uploadArticleImages, authToken: JSON.parse(localStorage.getItem('currentUser')).token });

    /**
     * hasBaseDropZoneOver is used by the FileUploadModule
     */
    public hasBaseDropZoneOver:boolean = false;

    /**
     * hasBaseDropZoneOver is used by the FileUploadModule
     */
    public hasAnotherDropZoneOver:boolean = false;

    /**
     * fileToken holds the JWT that are sent with the upload
     */
    fileToken: string

    /**
     * token holds the JWT
     */
    public token: string;

    /**
     * JwtHelper holds the JwtHelper class
     */
    jwtHelper: JwtHelper = new JwtHelper();

    /**
     * urlThisRoot holds the url to the root of this site
     */
    private urlThisRoot: string = environment.URL.thisRoot;

    /**
     * constructor
     *
     * Constructor for AddarticleComponent
     * @param router Router
     * @param articlesService  ArticlesService
     */
    constructor(
        private router: Router,
        private articlesService: ArticlesService) {
        }

  ngOnInit() {
  }

  /**
   * updateImageName
   *
   * update the image name so it can be used to make the path for the image
   * @param name:string the name of the image
   */
  updateImageName(name) {
    this.imageName = name;
  }

  /**
   * togleReady
   *
   * Togle the areYouReady value
   */
  togleReady() {
    if(!(this.areYouReady)){
      this.areYouReady = true;
    }
    else {
      this.areYouReady = false;
    }
    // console.log(this.areYouReady);
  }

  /**
   * myFunk
   *
   * @depricated
   * used for debugpurposes
   */
  myFunk(){
    // console.log(this.areYouReady);
  }

  /**
   * Add the article for the current user
   *
   * Add one article
   */
  addArticle() {
    let fileNamelist = []
    let localEmail = JSON.parse(localStorage.getItem('currentUser')).email

    let currentUser = localStorage.getItem('currentUser');
    let currentToken = JSON.parse(currentUser);
    let token = currentToken.token;
    // console.log(this.jwtHelper.decodeToken(token)._id);
    let localId = this.jwtHelper.decodeToken(token)._id;

    // console.log(this.model);
    this.model.tags = this.model.mytags.split(', ');
    // console.log(this.uploader)
    for (let item of this.uploader.queue){
      // console.log(item._file.name);
      fileNamelist.push(this.urlThisRoot + localId + ":"+ item._file.name)
      //this.model.imgLink.push(item._file.name);// = [JSON.parse(localStorage.getItem('currentUser')).email + ":" + this.imageName]
    }
    this.model.imgLink = fileNamelist;

    // console.log(this.model.imgLink);
      this.loading = true;
      this.articlesService.addArticle(this.model)
          .subscribe(result => {
            // console.log('Got response!');
              if (result === true) {
                  // might not auto redirect user here!
                  this.router.navigate(['/']);
              } else {
                  this.error = 'Can not add article at this moment, try again later';
                  this.loading = false;
              }
          });
  }

  /**
   * fileOverBase
   *
   * Used by the FileUploader
   * @param e:any
   */
  public fileOverBase(e:any):void {
    this.hasBaseDropZoneOver = e;
  }

  /**
   * fileOverAnother
   *
   * Used by the FileUploader
   * @param e:any
   */
  public fileOverAnother(e:any):void {
    this.hasAnotherDropZoneOver = e;
  }

}
