import { Component, OnInit, OnDestroy, EventEmitter, Output } from '@angular/core';
import { User } from '../_models/index';
import { AuthenticationService } from '../_services/authentication.service';
import { Subscription }   from 'rxjs/Subscription';



/**
 * UserPanelComponent
 *
 * Holds the controls of the user.
 *
 * Implements OnInit, OnDestroy
 */
@Component({
    selector: 'UserPanelComponent',
    templateUrl: './userpanel.component.html',
    styleUrls: ['./userpanel.component.css'],
})
export class UserPanelComponent implements OnInit, OnDestroy {
    @Output() onToggleSide = new EventEmitter<any>();
    private userSub;
    private user: User;

    /**
     * constructor
     *
     * Constructor for UserPanelComponent
     * Initializes variable user from the AuthenticationService
     * @param AuthenticationService  AuthenticationService
     * The AuthenticationService is injected into UserPanelComponent.
     */
    constructor(private service: AuthenticationService) {
      this.userSub = this.service.getCurrentUserObservable().subscribe(user => {
        this.user = user; // Subscribe and get user from the authService
      })
    }

    ngOnInit() {

    }
    ngOnDestroy() {
      this.userSub.unsubscribe();
    }



    /**
     * toggleSideNav
     *
     * Alert the parent component that it is time to toggle the sidenav
     * @oaram evt  event trigger
     */
    toggleSideNav(evt) {
      this.onToggleSide.emit(evt);
    }

    // TODO: fire the logout method in authentication service when logout is clicked
}
