/**
 * Created by sigveskaugvoll on 28.10.2016.
 */
import { Injectable } from '@angular/core';
import { Article } from '../_models/index';
import { Observable } from 'rxjs';


@Injectable()
export class ActiveArticleMapService {
  //service declarations
  private activeArticle: Article;


  //services
  getActiveArticle():Article {
    return this.activeArticle;
  }

  setActiveArticle(article: Article):void {
    this.activeArticle = article;
  }




}
