import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

// import { AuthenticationService } from './index';
import { AuthenticationService } from './authentication.service';
import { User } from '../_models/index';


/**
 * @depricated
 * UserService is responible for the retrival of all users in the database
 */
@Injectable()
export class UserService {
    constructor(
        private http: Http,
        private authenticationService: AuthenticationService) {
    }

    /**
     * @depricated
     *
     * getUsers
     *
     * get all users in the db. need to be loged in to do this.
     *
     * @return User[]   a list of users
     */
    getUsers(): Observable<User[]> {
        // add authorization header with jwt token
        let headers = new Headers({ 'Authorization': this.authenticationService.token });
        let options = new RequestOptions({ headers: headers });
        let url = 'http://localhost:2000/api/auth/test_auth_route';
        // get users from api
        return this.http.get(url, options)
            .map((response: Response) => {
              let myvar = response.json()
              // console.log(myvar)
              return []
            });
            // TODO: do some magic here
    }
}
