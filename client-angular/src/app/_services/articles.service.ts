import { Injectable } from '@angular/core';
import { Http, Headers, URLSearchParams, Response, RequestOptions } from '@angular/http';

import { Article } from '../_models/index';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import 'rxjs/add/operator/map'


/**
 * ArticlesService is responible for communication about article-data with
 * the API.
 */
@Injectable()
export class ArticlesService {
    // service declarations
    private token: string;
    private allArticles: Article[] = new Array<Article>();
    private totalKnownArticles: number = 0;
    private urlGetArticles: string = environment.URL.getArticles
    private urlAddArticle: string = environment.URL.addArticle
    private urlGetArticleById: string = environment.URL.getArticleById
    private urlGetRecentlyViewedArticles: string = environment.URL.getRecentlyViewedArticles




    /**
     * constructor
     *
     * Initializes variable token with data from localStorage
     * @param http   Http
     * Http is injected into the service.
     */
    constructor(private http: Http) {
        // set token if saved in local storage
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser && currentUser.token;
    }

    /**
     * getTotalKnownArticles
     *
     * returns the total known articles matching the filtering criteria
     * sent by the server.
     * @returns   number - total known articles
     */
    getTotalKnownArticles(): number {
      return this.totalKnownArticles;
    }

    checkOwner(article, randomOwners: string[]): string {
      // console.log("checkOwner called");
      // console.log(article);
      if(article._owner){
        // console.log("found owner!");
        return article._owner;
      }
      // console.log("finds random owner name");
      let index = Math.floor((Math.random() * randomOwners.length) + 1);
      return randomOwners[index];
    }

    /**
     * getArticles
     *
     * @param filterOptions   FilterOptions the filter options to be used
     * @param sortBy   string  TODO: CHANGE ME
     * @param index   number   the start index of the resultset we request
     *
     * @returns Observable<Article[]> returns an observable holding the
     * requested articles
     */
    getArticles(filterOptions, sortBy: string, fromIndex: number): Observable<Article[]> {

      // HOTFIX removes unessesary double login fire bug
      let currentUser = JSON.parse(localStorage.getItem('currentUser'));
      this.token = currentUser && currentUser.token;

      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', `${this.token}`);

      let params = new URLSearchParams();
      params.set('filterOptions', JSON.stringify(filterOptions));
      params.set('fromIndex', JSON.stringify(fromIndex));
      params.set('sorting', sortBy);

      return this.http.get(this.urlGetArticles, { headers, search: params, }).cache() // CACHE THE RESPONSE
        .map((response: Response) => {
          // if (response.status == 401){
          //   console.log("401 - Unatorized. You can not view this right now");
          //   return false;
          // }
          let json = response.json();
          let articles = json.articles;
          let totalArticles = json.totalArticles;

          // ONLY WIPE OUR LIST IF THE FROMINDEX IS 0!
          if (fromIndex == 0) {
            this.allArticles = new Array<Article>();
            this.totalKnownArticles = totalArticles;
            // console.log("totalArticles: " + totalArticles)
          }
          for (var article in articles) {
            let a = new Article();
            // please please be a better way..
            a.id = articles[article]._id;
            a.name = articles[article].name;
            a.intro = articles[article].intro;
            a.txt = articles[article].txt;
            a.imgLink = articles[article].imgLink;
            a.tags = articles[article].tags;
            a.owner = articles[article].owner;
            a.price = articles[article].price;
            a.catref = articles[article].catref;
            a.fromDate = new Date(articles[article].fromDate);
            a.toDate = new Date(articles[article].toDate);
            a.active = articles[article].active;
            a.location = {
              lat: articles[article].location.lat,
              lng: articles[article].location.lng
            };
            this.allArticles.push(a);
          };
          return this.allArticles;

      }).catch((error: any) => {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
          error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        // console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
      });
    }

    /**
     * addArticle
     *
     * Add one article with the currently loged in user as the owner.
     * @param article:Article   the article that shall be addicted
     *
     * @returns boolean   true if successfull false otherwise
     */
    addArticle(article: Article): Observable<boolean> {

          // HOTFIX removes unessesary double login fire bug
          let currentUser = JSON.parse(localStorage.getItem('currentUser'));
          this.token = currentUser && currentUser.token;


          let headers = new Headers();
          headers.append('Content-Type', 'application/json');
          headers.append('Authorization', `${this.token}`);

          let options = new RequestOptions({ headers: headers }); // Create a request option

          let url = this.urlAddArticle;
          return this.http.post(url, article, options)
            .map((response: Response) => {
              return true;
            }).catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if an;


    }

  /**
   * getArticleById
   * Function to query server for a article, based on the given article id
   *
   * @param id : A articles unique ID, created and stored by database
   * @returns {Observable<Article>} returns a observable object. that observes a Article object
   */
    getArticleById(id: string): Observable<Article> {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', `${this.token}`);

      let params = new URLSearchParams();
      params.set('articleID', JSON.stringify(id));

      // TODO: Fix URI
      return this.http.get(this.urlGetArticleById, { headers, search: params, }).cache() // CACHE THE RESPONSE
        .map((response: Response) => {
          let json = response.json(); // this is the response from the serer
          let article = json.article; //this is the response article from the serever
          let randomOwners = ['Donald','Mike','Thomas','Sophia','Emma','Olivia','Isabella','Aiden','Jackson','Ethan','Liam','Ava','Lily','Zoe','Chloe','Mia','Madison','Emily', 'Ella'];
          // create an article from the serve response
          let a = new Article();
          // please please be a better way..
          a.id        = article._id;
          a.name      = article.name;
          a.intro     = article.intro;
          a.txt       = article.txt;
          a.imgLink   = article.imgLink;
          a.tags      = article.tags;
          a.owner     = this.checkOwner(article, randomOwners);
          a.price     = article.price;
          a.catref    = article.catref;
          a.fromDate  = new Date(article.fromDate);
          a.toDate    = new Date(article.toDate);
          a.active    = article.active;

          return a;

        }).catch((error: any) => {
          // In a real world app, we might use a remote logging infrastructure
          // We'd also dig deeper into the error to get a better message
          let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
          // console.error(errMsg); // log to console instead
          return Observable.throw(errMsg);
        });
    }


// --------------------------------------------------------------
//

/**
 * getRecentlyViewedArticles
 * Function to query server for a article, based on the given article id
 *
 * @param id : A articles unique ID, created and stored by database
 * @returns {Observable<Article>} returns a observable object. that observes a Article object
 */
  getRecentlyViewedArticles(): Observable<Article[]> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', `${this.token}`);

    // TODO: Fix URI
    return this.http.get(this.urlGetRecentlyViewedArticles, { headers }).cache() // CACHE THE RESPONSE
      .map((response: Response) => {
        let json = response.json(); // this is the response from the serer
        let articles = json.articles; //this is the response article from the serever
        let viewedArticles:Article[] = [];

        for (var article in articles) {
          let a = new Article();
          // please please be a better way..
          a.id = articles[article]._id;
          a.name = articles[article].name;
          a.intro = articles[article].intro;
          a.txt = articles[article].txt;
          a.imgLink = articles[article].imgLink;
          a.tags = articles[article].tags;
          a.owner = articles[article].owner;
          a.price = articles[article].price;
          a.catref = articles[article].catref;
          a.fromDate = new Date(articles[article].fromDate);
          a.toDate = new Date(articles[article].toDate);
          a.active = articles[article].active;
          a.location = {
            lat: articles[article].location.lat,
            lng: articles[article].location.lng
          };
          viewedArticles.push(a);
        };
        return viewedArticles;

      }).catch((error: any) => {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
          error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        // console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
      });
  }




}

/**
 * FilterOptions is an interface used to create a 'contract' for the
 * filterOptions used when requesting articles.
 */
export interface FilterOptions {
  id: string;
  name: string;
  intro: string;
  txt: string;
  tags: string[];
  owner: string;
  price: number[]; // range, from x to y
  catref: string; // TODO: fixme
  fromDate: Date;
  toDate: Date;
  active: boolean;
}
