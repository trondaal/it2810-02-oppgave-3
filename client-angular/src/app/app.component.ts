import { Component } from '@angular/core';
import { UserPanelComponent } from './userpanel/userpanel.component';
import { Router } from '@angular/router';



/**
 * AppComponent
 *
 * main app component, which is boostraped and contains our entire app.
 */
@Component({
  // moduleId: module.id,
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {}
