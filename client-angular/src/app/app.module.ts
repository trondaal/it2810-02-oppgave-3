import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule }    from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';


import { AgmCoreModule } from 'angular2-google-maps/core';

import { provideAuth } from 'angular2-jwt';

import {FileUploadModule} from 'ng2-file-upload';

import { AppComponent }  from './app.component';
import { routing }        from './app.routing';

import { AuthGuard } from './_guards/index';
import { AuthenticationService, UserService, ArticlesService } from './_services/index';
import { LoginComponent } from './login/index';
import { HomeComponent } from './home/index';
import { ArticlesComponent } from './articles/articles.component';
import { UserPanelComponent } from './userpanel/userpanel.component';
import { ArticleComponent, ArticleSmallComponent } from './article/index';
import { UsersettingsComponent } from './usersettings/index';
import { OrderByPipe } from './shared/pipes/orderBy';
import { RegisterComponent } from './register/register.component';
import { ArticlePageComponent} from './articlepage/index';
import { MapViewComponent } from './map/map.component';
import { AddarticleComponent } from './addarticle/addarticle.component';
import { RecentlyViewedArticlesComponent } from './recently-viewed-articles/recently-viewed-articles.component';
import { ActiveArticleMapService } from "./_services/active.map.service";
import { NavigationbarComponent } from './navigationbar/navigationbar.component';


/**
 * Tells angular how to compile and to run module code
 */
@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        MaterialModule.forRoot(),
        routing,
        FormsModule,
        ReactiveFormsModule,
        FileUploadModule,
    AgmCoreModule.forRoot({
        apiKey: 'AIzaSyAvkyyG7dASFJLicSyiODZxaTl4jIscTNM'
    })
    ],
    declarations: [
        AppComponent,
        LoginComponent,
        HomeComponent,
        ArticlesComponent,
        ArticleComponent,
        ArticleSmallComponent,
        UserPanelComponent,
        UsersettingsComponent,
        OrderByPipe,
        RegisterComponent,
        ArticlePageComponent,
        MapViewComponent,
        AddarticleComponent,
        RecentlyViewedArticlesComponent,
        NavigationbarComponent,
    ],
    providers: [
        AuthGuard,
        AuthenticationService,
        UserService,
        ArticlesService,
        ActiveArticleMapService,
        {provide: APP_BASE_HREF, useValue : '/' },
        provideAuth({
          headerName: 'Authorization',
          headerPrefix: '',
          tokenName: 'token',
          tokenGetter: (() => localStorage.getItem('currentUser.token')),
          globalHeaders: [{'Content-Type':'application/x-www-form-urlencoded'}],
          noJwtError: true,
          noTokenScheme: true
        })
    ],
    bootstrap: [AppComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class AppModule { }
