import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from '../_services/index';
import { User } from '../_models/user';


@Component({
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
})

/**
 * LoginComponent
 *
 * Handle login of users
 */
export class LoginComponent implements OnInit {
    model: User = new User();
    loading = false;
    error = '';

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService) { }

    /**
     * Reset loginstatus when the component is first created
     */
    ngOnInit() {
        // reset login status
        this.authenticationService.logout();
    }

    /**
     * Login the current user
     *
     * Requests the user to be logged in through the AuthenticationService.
     * The AuthenticationService then handles the actual logging in.
     */
    login() {
      // console.log(this.model);
        this.loading = true;
        this.authenticationService.login(this.model.email, this.model.password)
            .subscribe(result => {
              // console.log("Got response!")
                if (result === true) {
                    this.router.navigate(['/']);
                } else {
                    this.error = 'Email or password is incorrect';
                    this.loading = false;
                }
            });
    }
}
