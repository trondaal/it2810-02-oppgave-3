/**
 * Created by sigveskaugvoll on 20.10.2016.
 */

import { Component } from "@angular/core";
import {User} from "../_models/user";
import {Router} from "@angular/router";
import {AuthenticationService} from "../_services/authentication.service";


/**
 * RegisterComponent
 *
 * Holds logic for registering a new user  
 */
@Component ({
  selector: "RegisterComponent",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.css"]
})

/**
 * RegisterComponent
 *
 * Handle registering of users
 */
export class RegisterComponent {
  model: User = new User();
  loading = false;
  error = '';
  registered = false;

  constructor(
      //parameters for constructor
      private router: Router,
      private authenticationService: AuthenticationService) { }

  /**
   * Register the user-info as user
   *
   * Requests the user to be registered through the AuthenticationService.
   * The AuthenticationService then handles the actual registration.
   */

  register() {
    this.model.role = "Member";
    this.loading = true;
    this.authenticationService.register(this.model)
      .subscribe(result => {
        if(result === true) {
          this.registered = true;
        } else {
          this.error = "Something went wrong, please try again";
        }
      });



  }




}
