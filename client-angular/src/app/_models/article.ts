
/**
 * Article
 *
 * A Model holding article data
 */
export class Article {
  id: string;
  name: string;
  intro: string;
  txt: string;
  imgLink: string[];
  tags: string[];
  owner: string;
  price: number;
  catref: string;
  fromDate: Date;
  toDate: Date;
  active: boolean;
  lat: number;
  lng: number;
  mytags: string;
  location: {
    lat:number;
    lng:number;
  }
}
