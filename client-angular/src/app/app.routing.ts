import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/index';
import { HomeComponent } from './home/index';
import { AuthGuard } from './_guards/index';
import { UsersettingsComponent } from './usersettings/index';
import { RegisterComponent } from './register/register.component';

import { MapViewComponent } from './map/map.component';
import { AddarticleComponent } from './addarticle/addarticle.component';
import { ArticlePageComponent} from './articlepage/index';




const appRoutes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'add_article', component: AddarticleComponent, canActivate: [AuthGuard] },
    { path: 'my_account', component: UsersettingsComponent, canActivate: [AuthGuard] },
    { path: '', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'register', component: RegisterComponent },
    { path: 'map', component: MapViewComponent, canActivate: [AuthGuard]  },
    { path: 'article/:articleid', component: ArticlePageComponent, canActivate: [AuthGuard] },

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

/**
 * Routes
 *
 * Holds the path and component relation for each route in our application
 *
 * path login directs you to the login page
 *
 * path my_account directs you to the user settings page. Requires authentication.
 *
 * path '' (home) directs you to the home. Requires authentication.
 *
 * path register directs you to the register. Require authentication.
 *
 * path map directs you to the map page. Require authentication.
 *
 * path article/:articleid  directs you to a specified article. Require authentication. 
 *
 * path ** (any path other than the aforementioned) directs you to the '' path.
 *
 * AuthGuard is used to verify authentication client-side. Proper authentication
 * is handled serverside
 *
 */
export const routing = RouterModule.forRoot(appRoutes);
