import {Component, OnInit} from '@angular/core';

import { Article } from '../_models/index';

import { ActivatedRoute } from "@angular/router";
import { ArticlesService } from "../_services/articles.service";
import { Location } from '@angular/common';



/**
 * ArticlePageComponent
 *
 * Display one article
 */
@Component({
  selector: 'ArticlePageComponent',
  templateUrl: './articlepage.component.html',
  styleUrls: ['./articlepage.component.css'],
})

export class ArticlePageComponent implements OnInit{

  /**
   * article input, of type Article Model. injected into the component.
   */
  article : Article;

  constructor(
    private route : ActivatedRoute,
    private articleService : ArticlesService,
    private _location : Location
  ) {}

  /**
   * ngOnInit
   * Function that get called when this component is created.
   * Requests the clicked article from the database and sets the article to show
   * equal to the components article.
   *
   */
  ngOnInit(){
    let articleID = this.route.snapshot.params["articleid"];  // get the article id from the url


    this.articleService.getArticleById(articleID) // we filter on the article id, sort by price and returns the first in the sorted result.
      .subscribe(article => {
        this.article = article;

      });
  }

  onBackClick(){
    // console.log("clicked back button!!");
    this._location.back();

  }


}
