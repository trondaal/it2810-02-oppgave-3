var gulp = require('gulp');
var typedoc = require("gulp-typedoc");
gulp.task("default", function() {
    return gulp
        .src(["src/app/"])
        .pipe(typedoc({
            module: "commonjs",
            target: "es6",
            out: "docs/",
            name: "Del.nu",
            ignoreCompilerErrors: true,
            exclude: "**/*+(e2e|spec|index).ts",
            excludeExternals:true,
            excludeNotExported:true,
            experimentalDecorators: true,
        }))
    ;
});
