# Architecture
<img src="../architecture.png" alt=""/>

# How to run project on your computer
Install dependencies:
- [Node.js](https://nodejs.org/en/)
- [MongoDB](https://www.mongodb.com/)

Start mongoDB Server:
	`mongod`

Clone the repo:
	`git@bitbucket.org:trondaal/it2810-02-oppgave-3.git`

Go into folder “_it2810-02-oppgave-3/server_” and run:
`npm install
`
This may take some time.


Then start the server:
`npm start
`

The server should now be up and running.

## Angular-client
Go into folder “_it2810-02-oppgave-3/client-angular_” and run
	`npm install`
This may take some time…

Then start the server
	`ng serve`

The server should now be up and running at [http://localhost:4200](http://localhost:4200).

<!-- ## React-client
Go into folder “_it2810-02-oppgave-3/client-react_” and run
	`npm install`
This may take some time…

Then start the server
	`npm start`

The server should now be up and running at [http://localhost:4321](http://localhost:4321/). -->

<!-- # Angular

This project was generated with [angular-cli](https://github.com/angular/angular-cli) version 1.0.0-beta.17.

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Deploying to Github Pages

Run `ng github-pages:deploy` to deploy to Github Pages.

## Further help

To get more help on the `angular-cli` use `ng --help` or go check out the [Angular-CLI README](https://github.com/angular/angular-cli/blob/master/README.md). -->
