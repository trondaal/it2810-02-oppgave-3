# Project Description
[Del.nu](http://it2810-02.idi.ntnu.no:2000/) is a sharing community website where users can lend or rent articles of any* nature to one another. The website platform allows easy browsing of available articles to borrow or for rent, as well as [NYI] a structure of keeping track of articles you yourself have rented out or lent someone.

* we reserve the right to disallow any article or article-type, for past, current or future articles or article-types.

# Requirements
Here is a link to the requirements for this project (in norwegian) and short comments as to the implementations of them. Additionally we have listed libraries and guides used. 
https://drive.google.com/open?id=0Bzqr2bTVs3HYM2VTRHJvQndzZms


# Project structure
The project is split into two parts, the server part and the client part.
## Server
The server is the backbone for our website. 
Read it’s api-documentation here: [http://it2810-02.idi.ntnu.no:3000/docs/api-doc/delnu-api.html](http://it2810-02.idi.ntnu.no:3000/docs/api-doc/delnu-api.html)
## Client
The client is written with Angular using typescript.
Read its documentation here: `client-angular/docs/index.html` or http://it2810-02.idi.ntnu.no:3000/docs/index.html

# Descriptive text
We have also written down (in Norwegian) a more descriptive text of our project.
See: https://docs.google.com/document/d/1kXrTBDv1IAxiUxFpjjfdjvT0IKioh8xX9cZvAMl17TM/edit